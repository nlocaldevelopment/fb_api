require "koala"
require "fb_api/version"
require 'active_support/all'

require 'fb_api/koala_methods'
require 'fb_api/fb_object'
require 'fb_api/helpers'
require 'fb_api/raw_fb_object'
require 'fb_api/raw_mkt_object'
require 'fb_api/fb_object_collection'

module FbApi
  # Your code goes here...
end

Gem.find_files("fb_api/models/**/*.rb").each { |path| require path}
