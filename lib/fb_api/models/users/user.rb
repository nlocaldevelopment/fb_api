module FbApi
  class User < FbApi::RawFbObject
    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/graph-api/reference/page/)
    known_keys :id,
               :email,
               :token_for_business


    #ext_keys

    list_path   'me'

    def self.standalone
      true
    end

    #def self.current_id= (account_id)
    #  Thread.current[:current_user]= account_id
    #end

    #def self.current_id
    #  Thread.current[:current_user]||= self.try(:all).try(:first).try(:id)
    #end

 end
end
