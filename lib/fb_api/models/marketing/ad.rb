module FbApi
  class Ad < ::FbApi::RawMktObject
    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/reference/adgroup)
    known_keys :id,
               :account_id,
               :adset_id,
               :bid_amount,
               :campaign_id,
               :created_time,
               :creative,
               :effective_status,
               :last_updated_by_app_id,
               :name,
               :status,
               :updated_time
               #:daily_budget

    #ext_keys   :ad_review_feedback,
    #           :adlabels,
    #           :bid_info,
    #           :bid_type,
    #           :conversion_specs,
    #           :recommendations,
    #           :tracking_specs,
    #           :leads,
    #           :keywordstats,
    #           :reachestimate,
    #           :targetingsentencelines,
    #           :adcreatives

    parent_object    :ad_set, as: :adset_id
    list_path        :ads
    connections      :insights, :ad_creatives, :leads

  end
end
