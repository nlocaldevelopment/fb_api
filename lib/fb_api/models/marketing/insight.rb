module FbApi
  class Insight < ::FbApi::RawMktObject
    # Known keys as per
    # the [fb docs](https://developers.facebook.com/docs/marketing-api/reference/ad-campaign-group)
    # as well as undocumented keys returned by the Graph API
    known_keys :account_id,
               :account_name,
               :action_values,
               :actions,
               :ad_id,
               :ad_name,
               :adset_id,
               :adset_name,
               #:app_store_clicks,
               :buying_type,
               #:call_to_action_clicks,
               :campaign_id,
               :campaign_name,
               :canvas_avg_view_percent,
               :canvas_avg_view_time,
               :clicks,
               :cost_per_action_type,
               :cost_per_unique_action_type,
               :cost_per_10_sec_video_view,
               :cost_per_estimated_ad_recallers,
               :cost_per_inline_link_click,
               :cost_per_inline_post_engagement,
               #:cost_per_total_action,
               :cost_per_unique_click,
               :cost_per_unique_inline_link_click,
               :cpc,
               :cpm,
               :cpp,
               :ctr,
               :date_start,
               :date_stop,
               #:deeplink_clicks,
               :estimated_ad_recall_rate,
               :estimated_ad_recallers,
               :frequency,
               :impressions,
               :inline_link_click_ctr,
               :inline_link_clicks,
               :inline_post_engagement,
               #:newsfeed_avg_position,
               #:newsfeed_clicks,
               #:newsfeed_impressions,
               :objective,
               #:place_page_name,
               #:product_id,
               :reach,
               #:relevance_score,
               #:social_clicks,
               #:social_impressions,
               #:social_reach,
               :social_spend,
               :spend,
               #:total_action_value,
               #:total_actions,
               #:total_unique_actions,
               :unique_actions,
               :unique_clicks,
               :unique_ctr,
               #:unique_impressions,
               :unique_inline_link_click_ctr,
               :unique_inline_link_clicks,
               :unique_link_clicks_ctr,
               #:unique_social_clicks,
               #:unique_social_impressions,
               :video_10_sec_watched_actions,
               #:video_15_sec_watched_actions,
               :video_30_sec_watched_actions,
               #:video_avg_pct_watched_actions,
               #:video_avg_sec_watched_actions,
               #:video_complete_watched_actions,
               :video_p100_watched_actions,
               :video_p25_watched_actions,
               :video_p50_watched_actions,
               :video_p75_watched_actions,
               :video_p95_watched_actions,
               #:website_clicks,
               :website_ctr


    list_path  :insights

    def self.all(graph: FbApi.graph, parent: nil, args: {:sort => "reach_descending",:level => "ad",:time_increment => 1, :date_preset => "yesterday"}, opts: default_opts)
      super
    end

    def parent_object
      @parent_object
    end

  end
end
