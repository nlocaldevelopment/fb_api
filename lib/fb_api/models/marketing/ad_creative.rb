module FbApi
  class AdCreative < ::FbApi::RawMktObject

    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/reference/ad-creative)
    known_keys :id,
               :name

    ext_keys   :actor_id,
               :actor_image_hash,
               :actor_image_url,
               :actor_name,
               :applink_treatment,
               :body,
               :call_to_action_type,
               :image_hash,
               :image_url,
               :instagram_actor_id,
               :instagram_permalink_url,
               :instagram_story_id,
               :link_og_id,
               :link_url,
               :platform_customizations,
               :object_story_spec,
               :object_id,
               :object_story_id,
               :object_type,
               :object_url,
               :product_set_id,
               :run_status,
               :template_url,
               :thumbnail_url,
               :title,
               :url_tags,
               :image_crops,
               :adlabels

    parent_object :ad
    list_path     :adcreatives

    def account_id
      self.ad.account_id
    end

  end
end
