module FbApi
  class AdTrackingTag < ::FbApi::RawMktObject
    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/click-tags/v2.6)
    known_keys :id,
               :url,
               :adgroup_id

    parent_object    :ad
    list_path        :trackingtag

  end
end
