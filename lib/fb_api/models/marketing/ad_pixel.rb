module FbApi
  class AdPixel < ::FbApi::RawMktObject

    # Known keys as per
    # the [fb docs](https://developers.facebook.com/docs/marketing-api/reference/ad-campaign-group)
    # as well as undocumented keys returned by the Graph API
    known_keys :id,
               :name,
               :description,
               :type

    parent_object :ad_account, as: :account_id
    list_path  :adspixels

  end
end
