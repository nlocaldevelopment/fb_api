module FbApi
  class Activity < ::FbApi::RawMktObject
    known_keys :data

    list_path  :activities
  end
end
