module FbApi
  class Lead < ::FbApi::RawMktObject
    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/reference/adgroup)
    known_keys :id,
               :ad_id,
               :ad_name,
               :adset_id,
               :adset_name,
               :campaign_id,
               :campaign_name,
               :created_time,
               :custom_disclaimer_responses,
               :field_data,
               :form_id,
               :is_organic,
               :post

    parent_object    :ad, as: :ad_id
    list_path        :leads

  end
end
