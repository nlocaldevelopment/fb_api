module FbApi
  class AdSet < ::FbApi::RawMktObject

    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/reference/ad-campaign)
    known_keys :id,
               :name,
               :account_id,
               :campaign_id,
               :bid_amount,
               :billing_event,
               :effective_status,
               :end_time,
               #:frequency_cap,
               #:frequency_cap_reset_period,
               #:is_autobid,
               :created_time,
               :updated_time,
               :optimization_goal,
               :promoted_object,
               :start_time,
               :daily_budget,
               :status,
               :targeting
    ext_keys   :adlabels,
               :adset_schedule,
               :bid_info,
               :budget_remaining,
               :frequency_control_specs


    parent_object :ad_campaign, as: :campaign_id
    list_path     :adsets
    connections   :ads, :ad_creatives, :insights, :activities

  end
end
