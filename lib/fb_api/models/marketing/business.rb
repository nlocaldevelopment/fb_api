module FbApi
  class Business < ::FbApi::RawMktObject
    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/reference/ad-account)
    known_keys :id,
               :name,
               :timezone_id,
               :primary_page,
               :update_time,
               :updated_by,
               :creation_time,
               :created_by


    list_path   'me/businesses'
    connections :ad_accounts


    def self.standalone
      true
    end

    def self.current_id= (account_id)
      Thread.current[:current_ad_account]= account_id
    end

    def self.current_id
      Thread.current[:current_ad_account]||= ::AdAccount.by_allowed(true).try(:first).try(:id)
    end

  end
end
