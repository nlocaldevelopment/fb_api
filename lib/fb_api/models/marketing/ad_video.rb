module FbApi
  class AdVideo < ::FbApi::RawMktObject

    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/advideo/v2.6)
    # [fb_docs_videos] (https://developers.facebook.com/docs/graph-api/reference/video)
    known_keys :id,
               :backdated_time,
               :backdated_time_granularity,
               :created_time,
               :description,
               :embed_html,
               :embeddable,
               :format,
               :from,
               :icon,
               :content_category,
               :content_tags,
               :event,
               :is_crossposting_eligible,
               :is_instagram_eligible,
               :is_reference_only,
               :length,
               :live_status,
               :permalink_url,
               :picture,
               :place,
               :privacy,
               :published,
               :scheduled_publish_time,
               :source,
               :status,
               :title,
               :updated_time

    parent_object :ad_account, as: :account_id
    list_path     :advideos

  end
end
