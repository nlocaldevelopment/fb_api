module FbApi
  class AdImage < ::FbApi::RawMktObject

    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/reference/ad-image)
    known_keys :hash,
               :account_id,
               :id

    parent_object :ad_account, as: :account_id
    list_path     :adimages

    #Las AdImages funciona distinto al resto de objetos de facebook:
    #En lugar de accederse directamente por su id, hay que hacer llamadas al path
    #de listado, pasando como parametro su hash. Ademas el formato de respuesta
    #varía para algunos metodos. Lo cual hace necesario redefinirlos.

    def self.create (graph, data, parent=nil, path=nil)
      raise_if_read_only
      p = path || parent.path

      # We want facebook to return the data of the created object
      data["redownload"] = 1

      # Create
      result = create_connection(graph: graph, parent: p, path: list_path, data: data)

      if d=result['images']["bytes"]
        data = d
      # Don't know what to do. No id and no data. I need an adult.
      else
        raise "Invalid response received, found neither a data nor id key in #{result}"
      end

      # Return a new instance
      new(graph, data, parent)
    end

    # @return [String] Most facebook objects will need to return their
    #   id property here, so that's the default. Overwrite if necessary
    def path
      self [:hash] or raise "Can't find a path unless I have a hash #{self.inspect}"
      path_with_parent(self.ad_account)
    end


    def default_args
      super.merge!({hash: self[:hash]})
    end

    def default_opts
      self.class.default_opts
    end

    def self.default_opts
      super.merge!({preserve_form_arguments: true})
    end

    private

    def get(graph:, path:, args:default_args, options:default_ops, account_id:)
      begin
        graph.graph_call(path.to_s, args , "get", options ) { |result|
           result.kind_of?(Array) ? result.first : result
        }
      rescue => e
        if e.kind_of?(::Koala::Facebook::ClientError) 
          case e.fb_error_code.to_i
          when 4
            FbApi::Mutex.block(":global")
          when 17
            account_id ||= self.base_account_type.current_id
            FbApi::Mutex.block(account_id)
          when 32
            account_id ||= FbApi::Page.current_id
            FbApi::Mutex.block(account_id)
          end
        end
        puts "#{e} graph.get_object(#{path.to_json})" if in_irb?
        raise e
      end
    end
  end
end
