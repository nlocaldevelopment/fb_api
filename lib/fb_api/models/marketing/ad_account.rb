module FbApi
  class AdAccount < ::FbApi::RawMktObject
    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/marketing-api/reference/ad-account)
    known_keys :id,
               :account_id,
               :account_status,
               :age,
               :amount_spent,
               :balance,
               :business_city,
               :business_country_code,
               :business_name,
               :business_state,
               :business_street,
               :business_street2,
               :business_zip,
               :can_create_brand_lift_study,
               :capabilities,
               :created_time,
               :currency,
               :disable_reason,
               :end_advertiser,
               :end_advertiser_name,
               :funding_source,
               :has_migrated_permissions,
               :io_number,
               :is_notifications_enabled,
               :is_personal,
               :is_prepay_account,
               :is_tax_id_required,
               :line_numbers,
               :media_agency,
               :min_campaign_group_spend_cap,
               :min_daily_budget,
               :name,
               :offsite_pixels_tos_accepted,
               :owner,
               :partner,
               :spend_cap,
               :tax_id,
               :tax_id_status,
               :tax_id_type,
               :timezone_id,
               :timezone_name,
               :timezone_offset_hours_utc,
               :tos_accepted,
               :user_role

    ext_keys   :agency_client_declaration,
               :business,
               :failed_delivery_checks,
               :funding_source_details,
               :rf_spec

    list_path   'me/adaccounts'
    connections :ad_campaigns, :ad_sets, :ads, :ad_creatives, :ad_images, :ad_videos, :insights, :ad_pixels

    def path
      normalize_account_id(id)
    end

    def set_data(data)
      super
      self.id = normalize_account_id(id)
    end


    def self.standalone
      true
    end

    def self.current_id= (account_id)
      Thread.current[:current_ad_account]= account_id
    end

    def self.current_id
      Thread.current[:current_ad_account]||= self.try(:all).try(:first).try(:id)
    end

  end
end
