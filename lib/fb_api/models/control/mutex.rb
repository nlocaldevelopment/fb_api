module FbApi
  class Mutex
    attr_accessor :id

    def initialize(id: FbApi::AdAccount.current_id)
      @id= id
    end

    def blocked?
      self.if ? self.mutex.locked? : false
    end

    def block
      self.mutex.lock if self.id
    end

    def mutex
      @mutex||=RedisMutex.new(self, block: 0, expire: 1.1.minutes )
    end

    def self.blocked?(object_id)
      object_id ? self.new(id: object_id).mutex.locked? : false
    end

    def self.block(object_id)
      self.new(id: object_id).mutex.lock if object_id
    end
  end
end
