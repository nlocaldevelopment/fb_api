module FbApi
  class Page < FbApi::RawFbObject
    # Known keys as per
    # [fb docs](https://developers.facebook.com/docs/graph-api/reference/page/)
    known_keys :id,
               :about,
               :access_token,
               :link,
               :name


    #ext_keys

    list_path   'me/accounts'

    def self.all(graph:(User.find_by_id(FbApi::User.current_id).try(:graph) || FbApi.graph))
      super(graph)
    end

    def self.standalone
      true
    end

    def graph
      @graph ||= ::Koala::Facebook::API.new(self.access_token)
    end

    def subscribe_app
     page_graph.graph_call(path+"/subscribed_apps", {}, "post", {})["success"]
    end

    def subscribed_apps
     page_graph.graph_call(path+"/subscribed_apps", {}, "get", {})
    end

    def self.current_id= (page_id)
      Thread.current[:current_account]= page_id
    end

    def self.current_id
      Thread.current[:current_account]||= ::AdAccount.by_allowed(true).try(:first).try(:id)
    end

 end
end
