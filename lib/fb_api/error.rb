module FbApi
  class FbApiError < StandardError; end
  module Error

    class ReadOnly < ::FbApi::FbApiError; end

  end
end
