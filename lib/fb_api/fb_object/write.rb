module FbApi
 module FbObject
   module Write
    extend ActiveSupport::Concern
    include FbApi::Helpers

    def self.included(base)
      base.extend(ClassMethods)
    end

    def save (opts=default_opts)
      self.class.raise_if_read_only

      # Tell facebook to return
      data = @hash_delegator_hash.merge(redownload: true)
      data = data.stringify_keys

      # Don't post ids, because facebook doesn't like it
      data = data.keep_if{ |k,v| k != "id" && !v.blank?}
      # Update or create on facebook
      if !self[:id].blank?
        post(graph: graph, path: path, data: data, opts: opts)
      else
        result= self.class.create(graph, data, FbApi::AdAccount.find(normalize_account_id(self.account_id)), nil, opts)
        self.set_data(result.data)
      end
      self
    end

    def destroy(args=default_args, opts=default_opts)
      self.class.destroy(graph: graph, path: path, args: args, opts: opts, account_id: self.try(:account_id) || @parent_object.try(:account_id) )
    end


    module ClassMethods

      def raise_if_read_only
        return unless self.read_only?
        raise FbApi::Error::ReadOnly.new("#{self} is read_only")
      end

      def create(graph, data, parent=nil, path=nil, opts=default_opts)
        raise_if_read_only
        p = path || (parent ? parent.path : nil)

        # We want facebook to return the data of the created object
        data["redownload"] = true

        # Create
        result = create_connection(graph: graph, parent: p, path: list_path, data: data, opts: opts)

        # If the redownload flag was supported, the data is nested by
        # name and id, e.g.
        #
        #     "campaigns" => { "12345" => "data" }
        #
        # Since we only create one at a time, we can just say:
        if d=result['data']
          data = d.values.first.values.first
          object = new(graph, data, parent)
        # Redownload was not supported, in this case facebook returns
        # just {"id": "12345"}
        elsif result['id']
          data = result
          object =  new(graph, data, parent).reload
        # Don't know what to do. No id and no data. I need an adult.
        else
          raise "Invalid response received, found neither a data nor id key in #{result}"
        end

        # Return a new instance
        object
      end

      def destroy(graph:, path:, args:default_args, opts:default_opts, account_id:)
        raise_if_read_only
        delete(graph: graph, path: path, args: args, opts: opts)
      end

    end

   end
 end
end
