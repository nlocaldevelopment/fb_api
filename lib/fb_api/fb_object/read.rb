module FbApi
 module FbObject
   module Read
    extend ActiveSupport::Concern

    def self.included(base)
      base.extend(ClassMethods)
    end

    # @param graph [::Koala::Facebook::API] A graph with access_token
    # @param data [Hash] The properties you want to assign, this is what
    #   facebook gave us (see known_keys).
    # @param parent [<FbObject] A parent context for this class, must
    #   inherit from {FbApi::FbObject}
    def initialize(graph, data = {}, parent=nil)
      self.graph = graph
      set_data(data)
      begin
       set_parent(parent)
      rescue => e
        !parent.kind_of?(FbApi::AdAccount) ? raise(e) : ''
      end
    end

    # Refetches the data from facebook
    def reload (args=default_args,opts=default_opts)
      data = get(graph: graph, path: path, args: args, opts: opts, account_id: self.try(:account_id) || @parent_object.try(:account_id))
      self.set_data(data)
      self
    end

    private

    # Sets the parent of this instance
    #
    # @param parent [FbObject] Has to be of the same class type you defined
    #   using {FbObject.parent_object}
    def set_parent(parent)
      return unless parent
      self.class.validate_parent_object_class(parent)
      @parent_object = parent
    end

    module ClassMethods

      # Create a new chainable scope
        #
        # @example
        #   class User
        #     include Her::Model
        #
        #     scope :admins, lambda { where(:admin => 1) }
        #     scope :page, lambda { |page| where(:page => page) }
        #   enc
        #
        #   User.admins # Called via GET "/users?admin=1"
        #   User.page(2).all # Called via GET "/users?page=2"
        def scope(name, code)
          # Add the scope method to the class
          (class << self; self end).send(:define_method, name) do |*args|
            instance_exec(*args, &code)
          end

          # Add the scope method to the RawFbObject class
          RawFbObject.instance_eval do
            define_method(name) { |*args| instance_exec(*args, &code) }
          end
        end

        # @private
        def scoped
          @_fb_default_scope || blank_relation
        end

        # Define the default scope for the model
        #
        # @example
        #   class User
        #     include Her::Model
        #
        #     default_scope lambda { where(:admin => 1) }
        #   enc
        #
        #   User.all # Called via GET "/users?admin=1"
        #   User.new.admin # => 1
        def default_scope(block=nil)
          @_fb_default_scope ||= (!respond_to?(:default_scope) && superclass.respond_to?(:default_scope)) ? superclass.default_scope : scoped
          @_fb_default_scope = @_her_default_scope.instance_exec(&block) unless block.nil?
          @_fb_default_scope
        end

        # Delegate the following methods to `scoped`
        [:all, :where, :create, :find].each do |method|
          class_eval <<-RUBY, __FILE__, __LINE__ + 1
           def #{method}(*params)
              scoped.send(#{method.to_sym.inspect}, *params)
            end
          RUBY
        end


      # Finds by object id and checks type
      def find(id, graph = FbApi.graph, args=default_args, opts=default_opts)
        new(graph, id: id).reload(args,opts)
      end

      # Add a query string parameter
      def where(params = {})
        return self if params.blank?
        result= self.all.reject { |c|
            params.any? {|key, value|
              c.send(key) != value
            }
        }
      end


      # Automatique all getter.
      #
      # Let's say you want to fetch all campaigns
      # from facebook. This can happen in the context of an ad
      # account. In this gem, that context is called a parent. This method
      # would only be called on objects that inherit from {FbObject}.
      # It asks the `parent` for it's path (if it is given), and appends
      # it's own `list_path` property that you have defined (see
      # list_path)
      #
      # If, however, you want to fetch all ad creatives, regardless of
      # which ad group is their parent, you can omit the `parent`
      # parameter. The creatives returned by `FbApi::AdCreative.all` will
      # return `nil` when you call `#ad_group` on them, though, because facebook
      # will not return this information. So if you can, try to fetch
      # objects through their direct parent, e.g.
      # `my_ad_group.ad_creatives`.
      #
      # @param graph [::Koala::Facebook::API] A graph with access_token
      # @param parent [<FbObject] A parent object to scope
      def all(graph: FbApi.graph, parent: nil, args: default_args, opts: default_opts)
        parent ||= parent_ad_account_fallback
        r = get(graph: graph, path: path_with_parent(parent), args: args, opts: opts, account_id: parent.try(:account_id))
        ::FbApi::FbObjectCollection.new(::Koala::HTTPService::Response.new(400,r.raw_response.to_json, r.headers), r.api, self, graph, parent)
      end


      # Makes sure the given parent matches what you defined
      # in {FbObject.parent_object}
      def validate_parent_object_class(parent)
        resolve_parent_object_class
        e = "Invalid parent_object: #{parent.class} is not a #{@parent_object_class}"
        raise e if @parent_object_class and !parent.is_a?(@parent_object_class)
      end

      private


      # Attempts to resolve the {FbObject.parent_object} to a class at runtime
      # so we can load files in any random order...
      def resolve_parent_object_class
        return if @parent_object_class
        class_s = "FbApi::#{@parent_object_type.camelcase}" unless !@parent_object_type
        @parent_object_class = class_s ? class_s.constantize : nil
      end

      # Some objects can be fetched "per account" or "per parent
      # object", e.g. you can fetch all ad creatives for your account
      # or only for a special ad group.
      #
      # @return [nil, FbApi::FbObject] Returns the current ad account
      #   unless you're calling `FbApi::AdAccount.all`. Then we return
      #   nil because the ad account needs no parent.
      def parent_ad_account_fallback
        return nil if self.try(:standalone)
        FbApi::AdAccount.find(::FbApi::AdAccount.current_id)
      end

    end
   end
 end
end
