module FbApi
  module FbObject
    module Helpers

      def default_args
       @args ||= Rails.application.config.facebook["default_args"]
      end

      def default_opts
       @opts ||= {}
      end

      private

      def get(graph:, path:, args: default_args, opts: default_opts, account_id:)
        begin
          args= args.reverse_merge(default_args)
          opts= opts.reverse_merge(default_opts)
          args[:limit]  ||= Rails.application.config.facebook["requests"]["page_size"]
          args[:fields] ||= ((self.class == Class || !self.respond_to?(:keys)) ? self.base_keys.compact.join(',') : self.keys.compact.join(','))
          graph.graph_call(path, args, "get", opts)
        rescue => e
          if e.kind_of?(::Koala::Facebook::ClientError)
           case e.fb_error_code.to_i
           when 4
             FbApi::Mutex.block(":global")
           when 17
             account_id ||= self.base_account_type.current_id
             FbApi::Mutex.block(account_id)
           when 32
             account_id ||= FbApi::Page.current_id
             FbApi::Mutex.block(account_id)
           end
          end
          puts "#{e} graph.get_object(#{path.to_json})" if in_irb? && Rails.env == "development"
          raise e
        end
      end

      def create_connection(graph:, parent:, connection:, args: default_args, opts: default_opts, account_id:)
        begin
          args= args.reverse_merge(default_args)
          opts= opts.reverse_merge(default_opts)
          graph.put_connections(parent, connection, args, opts)
        rescue => e
          if e.kind_of?(::Koala::Facebook::ClientError)
            case e.fb_error_code.to_i
            when 4
              FbApi::Mutex.block(":global")
            when 17
              account_id ||= self.base_account_type.current_id
              FbApi::Mutex.block(account_id)
            when 32
              account_id ||= FbApi::Page.current_id
              FbApi::Mutex.block(account_id)
            end
          end
          msg = "#{e} graph.put_connections(#{parent.to_json}, #{connection.to_json}, #{args.to_json}, #{opts.to_json})"
          puts msg if in_irb? && Rails.env == "development"
          raise e
        end
      end

      def post(graph:, path:, data:, opts: default_opts, account_id:)
        begin
          opts= opts.reverse_merge(default_opts)
          graph.graph_call(path.to_s, data, "post", opts)
        rescue => e
          if e.kind_of?(::Koala::Facebook::ClientError)
            case e.fb_error_code.to_i
            when 4
              FbApi::Mutex.block(":global")
            when 17
              account_id ||= self.base_account_type.current_id
              FbApi::Mutex.block(account_id)
            when 32
              account_id ||= FbApi::Page.current_id
              FbApi::Mutex.block(account_id)
            when 1487225
              account_id ||= self.base_account_type.current_id
              FbApi::Mutex.block("#{account_id}:ad_creation")
            end
          end
          msg = "#{e} graph.graph_call(#{path.to_json}, #{data.to_json}, \"post\", #{opts.to_json})"
          puts msg if in_irb? && Rails.env == "development"
          raise e
        end
      end

      def delete(graph:, path:, args: default_args, opts: default_opts, account_id:)
        begin
          args = args.reverse_merge(default_args)
          opts = opts.reverse_merge(default_opts)
          args[:status]="DELETED"
          graph.graph_call(path, args, "delete", opts)
        rescue => e
          if e.kind_of?(::Koala::Facebook::ClientError)
            case e.fb_error_code.to_i
            when 4
              FbApi::Mutex.block(":global")
            when 17
              account_id ||= self.base_account_type.current_id
              FbApi::Mutex.block(account_id)
            when 32
              account_id ||= FbApi::Page.current_id
              FbApi::Mutex.block(account_id)
            end
          end
          puts "#{e} graph.delete(#{path.to_json})" if in_irb? && Rails.env == "development"
          raise e
        end
      end

      def path_with_parent(parent= nil)
        paths = []
        paths << parent.path if parent
        paths << self.list_path
        paths.join('/')
      end

      def in_irb?
        defined?(IRB)
      end

    end
  end
end
