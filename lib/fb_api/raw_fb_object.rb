require 'fb_api/koala_methods'
require 'fb_api/hash_delegator'
require 'fb_api/fb_object/helpers'
require 'fb_api/fb_object/dsl'
require 'fb_api/fb_object/read'
require 'fb_api/fb_object/write'

module FbApi
 class RawFbObject
   # A class that includes {FbApi::HashDelegator} for easy hash
   # access and default keys as methods as well as the `graph`
   # getter and setter from {FbApi::KoalaMethods}.
   #
   # By inheriting from this object, each fb object gets implemented
   # automatically (tm) through calling a couple of DSL methods and
   # defining how an object can obtain its own path.
   #
   # I feel it is example time, here's an imaginary ad campaign:
   #
   #     class AdCampaign < FbObject
   #
   #       known_keys    :title, :budget
   #       list_path     :adcampaigns
   #       connections   :ad_groups
   #       parent_object :ad_account, as: :account_id
   #
   #     end
   #
   # These handy things are now provided by {RawFbObject} to your object:
   #
   # 1.  Each `AdCampaign` object has a `title` and `budget` method. In
   #     case facebook returned more information than what's documented
   #     (there are a lot of these), you can still call
   #     `my_campaign[:secret_key]` to get to the juicy bits
   # 2.  You can call `AdCampaign.all(graph, my_ad_account)`, because your
   #     `AdCampaign` instance knows how to construct the path
   #     `act_12345/adcampaigns`. It knows this, because it knows its
   #     parent object and its own list path.
   # 3.  You can call `#ad_sets` on any `AdCampaign` instance to fetch
   #     the ad sets in that campaign. To add an ad_group to a campaign,
   #     you can call `AdSet.create(graph, data, my_campaign)`, or for
   #     short: `my_campaign.create_ad_set(data)`
   #

  extend  FbApi::FbObject::Helpers
  include FbApi::FbObject::Helpers
  include FbApi::HashDelegator
  include FbApi::KoalaMethods
  include FbApi::FbObject::DSL
  include FbApi::FbObject::Read
  include FbApi::FbObject::Write

  def self.base_account_type
    ::FbApi::Account
  end

 end
end
