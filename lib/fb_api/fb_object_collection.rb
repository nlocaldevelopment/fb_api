
module FbApi
  class FbObjectCollection < ::Koala::Facebook::API::GraphCollection
    attr_reader :object_type
    attr_reader :object_graph
    attr_reader :object_parent
    # Initialize the array of results and store various additional paging-related information.
    #
    # @param response the response from Facebook (a hash whose "data" key is an array)
    # @param api the Graph {::Koala::Facebook::API API} instance to use to make calls
    #            (usually the API that made the original call).
    #
    # @return [::Koala::Facebook::GraphCollection] an initialized GraphCollection
    #         whose paging, summary, raw_response, and api attributes are populated.
    def initialize(response, api, object_type= RawFbObject, object_graph = FbApi.graph, object_parent= nil)
      response.data["data"].map!{|c| object_type.new(object_graph, c, object_parent)}
      @object_type= object_type
      @object_graph= object_graph
      @object_parent= object_parent
      super response, api
    end

    # Retrieve the next page of results.
    #
    # @param [Hash] extra_params Some optional extra parameters for paging. For supported parameters see https://developers.facebook.com/docs/reference/api/pagination/
    #
    # @example With optional extra params
    #    wall = api.get_connections("me", "feed", since: 1379593891)
    #    wall.next_page(since: 1379593891)
    #
    # @return a GraphCollection array of additional results (an empty array if there are no more results)
    def next_page(extra_params = {})
      base, args = next_page_params
      base ? self.class.new(@api.get_page([base, args.merge(extra_params)]).raw_response,@api,@object_type,@object_graph,@object_parent) : nil
    end

    # Retrieve the previous page of results.
    #
    # @param [Hash] extra_params Some optional extra parameters for paging. For supported parameters see https://developers.facebook.com/docs/reference/api/pagination/
    #
    # @return a GraphCollection array of additional results (an empty array if there are no earlier results)
    def previous_page(extra_params = {})
      base, args = previous_page_params
      base ? self.class.new(@api.get_page([base, args.merge(extra_params)]).raw_response,@api,@object_type,@object_graph,@object_parent) : nil
    end

  end
end
